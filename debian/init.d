#! /bin/sh
### BEGIN INIT INFO
# Provides:          icegridnode
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Example initscript
# Description:       This file should be used to construct scripts to be
#                    placed in /etc/init.d.
### END INIT INFO

# Author: David Villa Alises <David.Villa@gmail.com>

# Do NOT "set -e"

# PATH should only include /usr/* if it runs after the mountnfs.sh script
PATH=/sbin:/usr/sbin:/bin:/usr/bin
DESC="ZeroC IceGrid node daemon"
NAME=icegridnode
DAEMON=/usr/bin/$NAME
PIDFILE=/var/tmp/$NAME.pid
DEFAULTS_FILE=/etc/default/$NAME
NODE_CONF="/etc/icegridnode.conf"
DAEMON_ARGS="--Ice.Config=$NODE_CONF --daemon --pidfile $PIDFILE"
SCRIPTNAME=/etc/init.d/$NAME

# Exit if the package is not installed
[ -x "$DAEMON" ] || exit 0

# Read configuration variable file if it is present
[ -r /etc/default/$NAME ] && . /etc/default/$NAME

# Load the VERBOSE setting and other rcS variables
. /lib/init/vars.sh

VERBOSE=1

# Define LSB log_* functions.
# Depend on lsb-base (>= 3.2-14) to ensure that this file is present
# and status_of_proc is working.
. /lib/lsb/init-functions


ADMIN_ARGS="--Ice.Config=$NODE_CONF -u $IG_USER -p $IG_PASS"

assert_enabled() {
    if [ $START_ICEGRIDNODE -ne 1 ]; then
        log_warning_msg "$NAME disabled: not starting. To enable it edit $DEFAULTS_FILE"
        exit 0
    fi
}

#
# Function that starts the daemon/service
#
do_start()
{
	# Return
	#   0 if daemon has been started
	#   1 if daemon was already running
	#   2 if daemon could not be started

        assert_enabled

        check_registry

	start-stop-daemon --start --quiet --pidfile $PIDFILE --exec $DAEMON --test > /dev/null || return 1
	start-stop-daemon --start --quiet --pidfile $PIDFILE --exec $DAEMON -- $DAEMON_ARGS || return 2
	# Add code here, if necessary, that waits for the process to be ready
	# to handle requests from services started subsequently which depend
	# on this one.  As a last resort, sleep for some time.
}

# FIXME: this line would have spaces around =
this_node_is_registry()
{
    grep "^IceGrid.Node.CollocateRegistry=1" $NODE_CONF > /dev/null
}

registry_online()
{
    icegridadmin $ADMIN_ARGS -e "registry list" 2> /dev/null > /dev/null
}

registry_host_resolved()
{
    host=$(python /usr/lib/icegridnode/get-locator-host.py --Ice.Config=$NODE_CONF)
    ping -c1 $host 2>&1 | grep -v "unknown host"  2> /dev/null > /dev/null
}

check_registry()
{
    if this_node_is_registry; then
	return 0
    fi

    if ! registry_host_resolved; then
	log_warning_msg "registry hostname is unknown right now, not starting"
	exit 0
    fi

    if ! registry_online; then
	log_warning_msg "couldn't reach the IceGrid registry right now"
    fi
}

shutdown_node()
{
    nodename=$(grep IceGrid.Node.Name /etc/icegridnode.conf | cut -d= -f2)
    icegridadmin $ADMIN_ARGS -e "node shutdown $nodename" 2> /dev/null
}

#
# Function that stops the daemon/service
#
do_stop()
{
	# Return
	#   0 if daemon has been stopped
	#   1 if daemon was already stopped
	#   2 if daemon could not be stopped
	#   other if a failure occurred

        shutdown_node
	RETVAL="$?"
	if [ "$RETVAL" != 0 ]; then
	    start-stop-daemon --stop --quiet --retry=TERM/30/KILL/5 --pidfile $PIDFILE --name $NAME
	    RETVAL="$?"
	    [ "$RETVAL" = 2 ] && return 2
	fi

	# FIXME: wait icegridnode process down
	sleep 2

	# Wait for children to finish too if this is a daemon that forks
	# and if the daemon is only ever run from this initscript.
	# If the above conditions are not satisfied then add some other code
	# that waits for the process to drop all resources that could be
	# needed by services started subsequently.  A last resort is to
	# sleep for some time.

#	start-stop-daemon --stop --quiet --oknodo --retry=0/30/KILL/5 --exec $DAEMON
#	[ "$?" = 2 ] && return 2

	# Many daemons don't delete their pidfiles when they exit.
	rm -f $PIDFILE
	return "$RETVAL"
}

#
# Function that sends a SIGHUP to the daemon/service
#
do_reload() {
	#
	# If the daemon can reload its configuration without
	# restarting (for example, when it is sent a SIGHUP),
	# then implement that here.
	#
	start-stop-daemon --stop --signal 1 --quiet --pidfile $PIDFILE --name $NAME
	return 0
}

case "$1" in
  start)
	[ "$VERBOSE" != no ] && log_daemon_msg "Starting $DESC" "$NAME"
	do_start
	case "$?" in
		0|1) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
		2) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
	esac
	;;
  stop)
	[ "$VERBOSE" != no ] && log_daemon_msg "Stopping $DESC" "$NAME"
	do_stop
	case "$?" in
		0|1) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
		2) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
	esac
	;;
  status)
	status_of_proc "$DAEMON" "$NAME" && exit 0 || exit $?
	;;
  #reload|force-reload)
	#
	# If do_reload() is not implemented then leave this commented out
	# and leave 'force-reload' as an alias for 'restart'.
	#
	#log_daemon_msg "Reloading $DESC" "$NAME"
	#do_reload
	#log_end_msg $?
	#;;
  restart|force-reload)
	#
	# If the "reload" option is implemented then remove the
	# 'force-reload' alias
	#
	log_daemon_msg "Restarting $DESC" "$NAME"
	do_stop
	case "$?" in
	  0|1)
		do_start
		case "$?" in
			0) log_end_msg 0 ;;
			1) log_end_msg 1 ;; # Old process is still running
			*) log_end_msg 1 ;; # Failed to start
		esac
		;;
	  *)
		# Failed to stop
		log_end_msg 1
		;;
	esac
	;;
  *)
	#echo "Usage: $SCRIPTNAME {start|stop|restart|reload|force-reload}" >&2
	echo "Usage: $SCRIPTNAME {start|stop|status|restart|force-reload}" >&2
	exit 3
	;;
esac

:
